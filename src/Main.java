import carinterface.Color;
import carinterface.YearOfManufacture;
import model.Bus;
import model.Car;
import model.PassengerCar;
import model.Tesla;

public class Main {
    public static void main(String[] args) {
        Car prius = new PassengerCar("Toyota Prius", YearOfManufacture.NEW_CAR,  Color.BLUE); // создание обьекта при помощи конструктора
        PassengerCar chaika = new PassengerCar("Чайка", YearOfManufacture.RARE_CAR,  Color.BLACK);
        Bus man = new Bus("Renault Laguna", YearOfManufacture.OLD_CAR, Color.GREEN);// от абстактного класса нелзя создать одноименный метод поэтому Car
        Tesla tesla = new Tesla("Tesla", YearOfManufacture.NEW_CAR, Color.WITH);
        // так три наследника победил... аж запутался кто и откуда =)

        tesla.start();
        tesla.move();
        tesla.say();
        tesla.stop();
        tesla.fire();
        tesla.addDistance(1359);
        System.out.println(tesla);

        System.out.println("");
        prius.start();
        prius.move();
        prius.stop();
        prius.fire();
        prius.addDistance(1500);
        System.out.println(prius);

        System.out.println("-------");
        System.out.println(chaika);
        chaika.start();
        chaika.move();
        chaika.stop();
        chaika.fire();
        chaika.addDistance(15000);
        System.out.println(chaika);

        System.out.println("-------");
        man.start();
        man.move();
        man.stop();
        man.fire();
        man.addDistance(35000);
        System.out.println(man);

    }
}
