package model;

import carinterface.Color;
import carinterface.YearOfManufacture;

public class Bus extends Car {

    public Bus(String name, YearOfManufacture year, Color color) {
        super(name, year, color);
    }

    @Override
    public void stop() {
        System.out.println("остановка...");
    }

    @Override
    public void fire() {
        System.out.println("автобус дальше не идет, все на выход!");
    }

    public void openDoor() {
        System.out.println("*Двери открываются*");
    }
    @Override
    public boolean isReadyToService() { // переопределение метода в наследуемом классе
        if(distanceOnService > 70000) {
            return true;
        }
        else {
            return false;
        }
    }
    @Override
    public void move(){
        System.out.println("остротожно двери закрываются, сдующая остановка ....");
    }

}
