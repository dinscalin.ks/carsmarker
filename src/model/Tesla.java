package model;

import carinterface.Color;
import carinterface.YearOfManufacture;

public class Tesla extends Car {

    public Tesla(String name, YearOfManufacture year, Color color) {
        super(name, year, color);
    }

    @Override
    public void stop() {
        System.out.println("вы прибыли в место назначения");
    }

    @Override
    public void fire() {
        System.out.println("Покупка нового автомобиля обойдется вам со скидкой примерно в 350мл$ XDD");
    }

    public void say() {
        System.out.println("*играет музыка*");
    }

    @Override
    public void move() {
        System.out.println("Здравствуйте хозяин, куда вас доставить?");
    }

    @Override
    public boolean isReadyToService() {
        return false;
    }
}
