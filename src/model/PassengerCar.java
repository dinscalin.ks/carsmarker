package model;

import carinterface.Color;
import carinterface.YearOfManufacture;


public class PassengerCar extends Car {

    public PassengerCar(String name, YearOfManufacture year, Color color) {
        super(name, year, color);
    }

    @Override
    public void stop() {
        System.out.println("мы прибыли");
    }

    @Override
    public void fire() {
        System.out.println("пакиньте автомобиль");
    }

    public void radio(){
        System.out.println("играет радио");
    }


    @Override
    public void move() {
        System.out.println("Уважаемые пассажиры, пристегнитесь =)");

    }

    @Override
    public boolean isReadyToService() {
        if(distanceOnService > 50000) {
            return true;
        }
        else {
            return false;
        }
    }

}
