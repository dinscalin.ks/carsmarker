package model;

import carinterface.Color;
import carinterface.Move;
import carinterface.Serviceable;
import carinterface.YearOfManufacture;

public abstract class Car implements Serviceable, Move {
    private String name;
    private YearOfManufacture year;
    private Color color;
    private int distance = 0;
    protected int distanceOnService = 0; // доступ к полю имеют только подклассы


    public Car(String name, YearOfManufacture year, Color color) { // конструктор с одним Enum-ом для простоты работы до 4й задачи
        this.name = name;                                                                  // тут используется int yearOfProduction
        this.color = color;
        this.year = year;   //подключил творой Enum
    }

    @Override
    public String toString() {
        return "Car{" +
                "name: '" + name + '\'' +
                ", Год производства -" + year.getI() +
                ", color - " + color.getStr1() + " - код цвета: " + color.getStr2() + // 30 мин убил на поиск - а чето у меня геттеров нет
                ", distance=" + getDistance() +
                ", distanceOnService=" + distanceOnService +
                '}';
    }

    public abstract void stop();

    public abstract void fire();

    public void addDistance(int additionDistance) { // увеличение дистанций общей и до следующего сервиса int
        distance += additionDistance;
        distanceOnService += additionDistance;
    }

    //public abstract boolean isReadyToService();   {  //TODO метод сравнения, будет переопределен в подклассах, переделалю его в абстрактный метод
       /* if(distanceOnService > 10000) {				//!!! в наследуемых классах обязательно должно быть прописано тело метода,
            return true;                           // !!! так как нет тела в абстрактном методе
        }											// * в подумал и перенес метот в интерфейс Serviceable
        return false;
    }*/

    @Override
    public int getDistanceOnService() {  // метод получение дистанции до сервиса
        return distanceOnService;
    }


    public int getDistance() {   //метод получения общей дистанции пробега
        return distance;
    }
}
