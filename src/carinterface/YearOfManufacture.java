package carinterface;

public enum YearOfManufacture {
    NEW_CAR(2023),
    OLD_CAR(2000),
    RARE_CAR(1963);

    private int i;
    YearOfManufacture(int i) {
        this.i = i;
    }

    public int getI() {
        return i;
    }

}
