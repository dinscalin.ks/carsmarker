package carinterface;

public interface Serviceable {
     void addDistance(int additionDistance);
     int getDistanceOnService();
     boolean isReadyToService();
}
