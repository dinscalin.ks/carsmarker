package carinterface;

public interface Move {
    default void start() {
        System.out.println("включение зажигания");
    }

    void move();

}
