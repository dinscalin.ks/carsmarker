package carinterface;

public enum Color {
    WITH("белый","#ffffff"),
    BLUE("голубой", "#3564fd"),
    BLACK("черный","#000000"),
    GREEN("зеленый", "#006400"),
    YELLOW("желтый", "#fff200");

    private String str1;
    private String str2;
    Color(String str1, String str2) {
        this.str1 = str1;
        this.str2 = str2;
    }

    public String getStr1() {
        return str1;
    }

    public String getStr2() {
        return str2;
    }
}
